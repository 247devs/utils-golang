package mailtrap

import (
	"net/smtp"
	"strings"
)

type Client struct {
	Username string
	Password string
}

func NewClient(username string, password string) *Client {
	return &Client{
		username,
		password,
	}
}

func (this *Client) SendEmail(from string, fromAlias string, to []string, subject string, headers []string, message string) (err error) {
	for _, address := range to {
		msg := "From: " + fromAlias + " <" + from + ">\n" +
			"To: " + address + "\n" +
			"Subject: " + subject + "\n"

		if len(headers) > 0 {
			msg += strings.Join(headers, "\n") + "\n"
		}

		msg += "\n" + message

		if err = smtp.SendMail(
			"smtp.mailtrap.io:2525",
			smtp.CRAMMD5Auth(this.Username, this.Password),
			from,
			[]string{address},
			[]byte(msg),
		); err != nil {
			return
		}
	}

	return
}