package config

import (
	"io/ioutil"
	"encoding/json"
	"strconv"
	"bitbucket.org/247devs/utils-golang/models"
	"errors"
	"os"
	"github.com/kelseyhightower/envconfig"
)

func GetConfig(config interface{}) (err error) {
	if configFilePath := os.Getenv("CONFIG_FILE_PATH"); len(configFilePath) == 0 {
		if err = envconfig.Process("", config); err != nil {
			return
		}
	} else {
		configFile, err := ioutil.ReadFile(configFilePath)
		if err != nil {
			return err
		}

		if err = json.Unmarshal(configFile, config); err != nil {
			return err
		}
	}

	return
}

type Port int
func (this Port) IsValid() error {
	if this <= 0 || this > 65535 {
		return errors.New("port is not valid, example: 4567")
	}

	return nil
}

func (this Port) String() string {
	return ":" + strconv.Itoa(int(this))
}

type DBName string
func (this DBName) IsValid() error {
	if len(this) == 0 {
		return errors.New("db name is not valid, example: `collection`")
	}

	return nil
}

func (this DBName) String() string {
	return string(this)
}

type DBUri string
func (this DBUri) IsValid() error {
	if len(this) == 0 {
		return errors.New("db uri is not valid, example: `mongodb://.....`")
	}

	return nil
}

func (this DBUri) String() string {
	return string(this)
}

type ServerConfig struct {
	ENV models.ENV `json:"env,omitempty" envconfig:"ENV"`
	Port Port `json:"port,omitempty" envconfig:"PORT"`
	DBName DBName `json:"dbName,omitempty" envconfig:"DB_NAME"`
	DBUri DBUri `json:"dbUri,omitempty" envconfig:"DB_URI"`
	EscobarPort Port `json:"escobarPort,omitempty" envconfig:"ESCOBAR_PORT"`
}

func (this ServerConfig) IsValid() (err error) {
	if err = this.ENV.IsValid(); err != nil {
		return
	}
	if err = this.Port.IsValid(); err != nil {
		return
	}
	if err = this.DBName.IsValid(); err != nil {
		return
	}
	if err = this.DBUri.IsValid(); err != nil {
		return
	}

	return nil
}