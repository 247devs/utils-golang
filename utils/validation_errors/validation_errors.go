package validation_errors

import (
	"reflect"

	"strings"

	"github.com/fatih/structs"
	"github.com/nicksnyder/go-i18n/i18n"
)

type ValidationErrorsInterface interface {
	GetValidationErrors() ValidationErrors
}

type ValidationErrorsWithTFuncInterface interface {
	GetValidationErrorsWithTFunc(T i18n.TranslateFunc) ValidationErrors
}

type ValidationErrorsWithTFuncAndInitialInterface interface {
	GetValidationErrorsWithTFuncAndInitial(T i18n.TranslateFunc, initialObj interface{}) ValidationErrors
}

func checkInterface(fieldPath fieldPath, T i18n.TranslateFunc, obj interface{}, initialObj interface{}) (errs ValidationErrors) {
	errs = ValidationErrors{}
	if vEInter, ok := obj.(ValidationErrorsInterface); ok {
		errs = vEInter.GetValidationErrors()
	} else if vEInter, ok := obj.(ValidationErrorsWithTFuncInterface); ok {
		errs = vEInter.GetValidationErrorsWithTFunc(T)
	} else if vEInter, ok := obj.(ValidationErrorsWithTFuncAndInitialInterface); ok {
		errs = vEInter.GetValidationErrorsWithTFuncAndInitial(T, initialObj)
	}

	for i, err := range errs {
		err.Field = fieldPath.AddPath(err.Field.String())
		errs[i] = err
	}

	return append(errs, getValidationErrors(fieldPath, T, obj, initialObj)...)
}

func GetValidationErrors(fieldPathPrefix string, T i18n.TranslateFunc, obj interface{}) (errs ValidationErrors) {
	return getValidationErrors(NewFieldPath(fieldPathPrefix), T, obj, obj)
}

func getValidationErrors(fieldPathPrefix fieldPath, T i18n.TranslateFunc, obj interface{}, initialObj interface{}) (errs ValidationErrors) {
	if structs.IsStruct(obj) {
		for _, f := range structs.Fields(obj) {
			if !(f.Kind() == reflect.Ptr && f.IsZero()) {
				errs = append(errs, checkInterface(fieldPathPrefix.AddPath(GetJsonTag(f)), T, f.Value(), initialObj)...)
			}
		}
	} else {
		val := reflect.ValueOf(obj)
		if val.Kind() == reflect.Interface && !val.IsNil() {
			elm := val.Elem()
			if elm.Kind() == reflect.Ptr && !elm.IsNil() && elm.Elem().Kind() == reflect.Ptr {
				val = elm
			}
		}

		if val.Kind() == reflect.Ptr {
			val = val.Elem()
		}

		if val.Kind() == reflect.Slice {
			for i := 0; i < val.Len(); i++ {
				v := val.Index(i)
				if v.Kind() != reflect.Ptr {
					v = v.Addr()
				}

				errs = append(errs, checkInterface(fieldPathPrefix.AddSlicePath(i), T, v.Interface(), initialObj)...)
			}
		}
	}

	return errs
}

func GetJsonTag(field *structs.Field) (jsonTag string) {
	if field != nil {
		jsonTag = strings.Split(field.Tag("json"), ",")[0]
		if len(jsonTag) == 0 {
			jsonTag = field.Name()
		}
	}

	return
}
