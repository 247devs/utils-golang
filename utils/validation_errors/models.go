package validation_errors

import (
	"strconv"
)

type ValidationError struct {
	Code    string    `json:"code,omitempty"`
	Message string    `json:"message,omitempty"`
	Field   fieldPath `json:"field,omitempty"`
}

type ValidationErrors []ValidationError

func (this *ValidationErrors) Append(vErr ValidationError) {
	if this == nil {
		this = &ValidationErrors{}
	}
	*this = append(*this, vErr)
}

type fieldPath string

func NewFieldPath(strFieldPath string) fieldPath {
	return fieldPath(strFieldPath)
}

func (this fieldPath) AddPath(str string) fieldPath {
	if len(str) == 0 {
		return this
	}

	if len(this) > 0 {
		this += "."
	}

	return this + fieldPath(str)
}

func (this fieldPath) AddSlicePath(index int) fieldPath {
	return this + "[" + fieldPath(strconv.Itoa(index)) + "]"
}

func (this fieldPath) String() string {
	return string(this)
}
