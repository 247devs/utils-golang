package localizer

import (
	"encoding/json"
	"strings"
)

type LocalizedString map[string]string

func (this *LocalizedString) Localize(lang string, returnAllLanguages bool) {
	if this == nil {
		return
	}

	if len(lang) == 0 {
		lang = "en"
	}

	val, _ := (*this)[lang]

	if !returnAllLanguages {
		*this = map[string]string{}
	}

	if len(val) > 0 {
		(*this)[LocalizeField] = val
	}
}

func (this LocalizedString) IsEqual(locString LocalizedString) bool {
	if len(this) != len(locString) {
		return false
	}

	for lang, text := range this {
		if t, ok := locString[lang]; !ok || t != text {
			return false
		}
	}

	return true
}

func (this LocalizedString) GetLocalValue() string {
	return this.GetLangValue(LocalizeField)
}

func (this LocalizedString) GetLangValue(lang string) string {
	val, _ := this[lang]
	return val
}

func (this *LocalizedString) SetLangValue(lang string, val string) {
	if this == nil {
		return
	}

	(*this)[strings.ToLower(lang)] = val
}

func (this *LocalizedString) UnmarshalJSON(b []byte) (err error) {
	type LS LocalizedString
	ls := LS(*this)
	if err = json.Unmarshal(b, &ls); err != nil {
		return
	}

	delete(ls, LocalizeField)

	(*this) = LocalizedString(ls)

	return
}

/*func (this *LocalizedString) GetBSON() (obj interface{}, err error) {
	if this == nil {
		return
	}

	delete(*this, LocalizeField)

	type LP LocalizedString
	lp := LP(*this)

	return bson.Marshal(&lp)
}*/
