package localizer

import (
	"reflect"

	"golang.org/x/text/language"
)

const LocalizeField = "localValue"

type Localizer interface {
	Localize(lang string, returnAllLanguages bool)
}

func Localize(lang interface{}, returnAllLanguage bool, obj interface{}) {
	langBase, _ := language.English.Base()

	switch v := lang.(type) {
	case language.Tag:
		langBase, _ = v.Base()
	case language.Base:
		langBase = v
	case string:
		tag, _, _ := language.ParseAcceptLanguage(v)
		if len(tag) > 0 {
			langBase, _ = tag[0].Base()
		}
	}

	localize(langBase, returnAllLanguage, obj)
}

func localize(lang language.Base, returnAllLanguage bool, obj interface{}) {
	if local, ok := obj.(Localizer); ok {
		local.Localize(lang.String(), returnAllLanguage)
		return
	}

	val := reflect.ValueOf(obj)
	if val.Kind() == reflect.Interface && !val.IsNil() {
		elm := val.Elem()
		if elm.Kind() == reflect.Ptr && !elm.IsNil() && elm.Elem().Kind() == reflect.Ptr {
			val = elm
		}
	}

	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	if val.Kind() == reflect.Struct {
		for i := 0; i < val.NumField(); i++ {
			field := val.Field(i)
			if field.Kind() != reflect.Ptr {
				field = field.Addr()
			}
			localize(lang, returnAllLanguage, field.Interface())
		}
	} else if val.Kind() == reflect.Slice {
		for i := 0; i < val.Len(); i++ {
			v := val.Index(i)
			if v.Kind() != reflect.Ptr {
				v = v.Addr()
			}
			localize(lang, returnAllLanguage, v.Interface())
		}
	}
}
