package number

import "math"

func RoundFloat(num float64, decimalPrecision int) float64 {
	mult := math.Pow(10, float64(decimalPrecision))
	return float64(int64(num*mult)) / mult
}
