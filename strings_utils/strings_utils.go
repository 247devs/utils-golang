package strings_utils

import (
	"time"
	mathRand "math/rand"
	cryptoRand "crypto/rand"
	"strings"
	"unicode"
	"regexp"
	"crypto/aes"
	"encoding/base64"
	"io"
	"crypto/cipher"
	"crypto/sha256"
	"fmt"
)

func GenerateRandomString(length int) (str string) {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	seededRand := mathRand.New(mathRand.NewSource(time.Now().UnixNano()))

	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func StringsToLowerWithoutSpace(s string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}

		return unicode.ToLower(r)
	}, s)
}

func GenerateNGrams(str1 string, str2 string) (ngram []string) {
	str1 = StringsToLowerWithoutSpace(str1)
	str2 = StringsToLowerWithoutSpace(str2)

	doNGrams := func(str string) []string {
		nGrams := make([]string, len(str))
		for i := 0; i < len(str); i++ {
			nGrams[i] = str[:(i+1)]
		}
		return nGrams
	}

	return append(doNGrams(str1 + str2), doNGrams(str2 + str1)...)
}

func CleanName(s string) string {
	s = strings.Map(func(r rune) rune {
		if !unicode.IsLetter(r) && !unicode.IsSpace(r) && r != '\'' {
			return -1
		}

		return unicode.ToLower(r)
	}, s)

	s = Trim(s)

	return strings.Title(s)
}

func Trim(s string) string {
	s = strings.TrimSpace(s)
	regex1 := regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`)
	regex2 := regexp.MustCompile(`[\s\p{Zs}]{2,}`)
	s = regex1.ReplaceAllString(s, "")
	return regex2.ReplaceAllString(s, " ")
}

func HashTo32Bytes(input string) []byte {
	data := sha256.Sum256([]byte(input))
	return data[0:]
}

func EncryptString(plainText string, keyString string) (cipherTextString string, err error) {

	key := HashTo32Bytes(keyString)
	encrypted, err := EncryptAES(key, []byte(plainText))
	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(encrypted), nil
}

func EncryptAES(key, data []byte) ([]byte, error) {

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	output := make([]byte, aes.BlockSize+len(data))
	iv := output[:aes.BlockSize]
	encrypted := output[aes.BlockSize:]

	if _, err = io.ReadFull(cryptoRand.Reader, iv); err != nil {
		return nil, err
	}

	stream := cipher.NewCFBEncrypter(block, iv)

	// note that encrypted is still a window in to the output slice
	stream.XORKeyStream(encrypted, data)
	return output, nil
}

func DecryptString(cryptoText string, keyString string) (plainTextString string, err error) {
	encrypted, err := base64.URLEncoding.DecodeString(cryptoText)
	if err != nil {
		return "", err
	}
	if len(encrypted) < aes.BlockSize {
		return "", fmt.Errorf("cipherText too short. It decodes to %v bytes but the minimum length is 16", len(encrypted))
	}

	decrypted, err := DecryptAES(HashTo32Bytes(keyString), encrypted)
	if err != nil {
		return "", err
	}

	return string(decrypted), nil
}

func DecryptAES(key, data []byte) ([]byte, error) {
	iv := data[:aes.BlockSize]
	data = data[aes.BlockSize:]

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	stream := cipher.NewCFBDecrypter(block, iv)

	stream.XORKeyStream(data, data)
	return data, nil
}