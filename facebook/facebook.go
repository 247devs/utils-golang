package facebook

import (
	"github.com/huandu/facebook"
)

type FacebookApp interface {
	ExchangeToken(accessToken string) (token string, expires int, err error)
	GetMe(accessToken string) (id string, name string, lastName string, email string, err error)
}

type facebookApp struct {
	App *facebook.App
}

func NewFacebookApp(appID, appSecret string) FacebookApp {
	return &facebookApp{facebook.New(appID, appSecret)}
}

func (this *facebookApp) ExchangeToken(accessToken string) (token string, expires int, err error) {
	return this.App.ExchangeToken(accessToken)
}

func (this *facebookApp) GetMe(accessToken string) (id string, name string, lastName string, email string, err error) {
	s := this.App.Session(accessToken)
	result, err := s.Get("/me", facebook.Params{
		"fields": "id,first_name,last_name,email",
	})
	if err != nil {
		return
	}

	id, _ = result["id"].(string)
	name, _ = result["first_name"].(string)
	lastName, _ = result["last_name"].(string)
	email, _ = result["email"].(string)

	return
}
