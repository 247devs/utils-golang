package facebook

type facebookAppMock struct{}

func NewFacebookAppMock() FacebookApp {
	return &facebookAppMock{}
}

func (this *facebookAppMock) ExchangeToken(accessToken string) (token string, expires int, err error) {
	return "long-lived-token", 10000, nil
}

func (this *facebookAppMock) GetMe(accessToken string) (id string, name string, lastName string, email string, error error) {
	return "12325212313", "Facebook Name", "Facebook lastName", "facebook@domain.com", nil
}
