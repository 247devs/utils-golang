package router

import (
	"errors"
	"net/http"

	"bitbucket.org/247devs/utils-golang/models"
	"github.com/zenazn/goji/web"
)

type ControllerFunc func(*Context) (interface{}, error)
type Middleware func(*Context)

type Router struct {
	Mux                 *web.Mux
	Middlewares         []Middleware
	prefix              string
	isOptionsReqEnabled bool
}

func NewRouter() *Router {
	return &Router{
		Mux: web.New(),
	}
}

func (this *Router) EnableOptionsRequests(enable bool) {
	this.isOptionsReqEnabled = enable
}

func (this *Router) AddPrefix(prefix string) {
	this.prefix += fixRouterPath(prefix)
}

func (this *Router) AddMiddleware(m ...Middleware) {
	this.Middlewares = append(this.Middlewares, m...)
}

func (this *Router) HandleFunc(f ControllerFunc, path string, method ...string) {
	if len(method) == 0 {
		method = []string{http.MethodGet}
	}
	if this.isOptionsReqEnabled {
		method = append(method, http.MethodOptions)
	}

	uniqueMethods := models.SliceString(method).GetUniquesStr()

	for _, m := range uniqueMethods {
		g := func(c web.C, w http.ResponseWriter, r *http.Request) {
			context := NewContext(
				r,
				w,
				c.URLParams,
				this.Middlewares,
				f,
			)

			context.Do()

			w.Header().Set("Request-Id", context.Id)
			if context.StatusCode != http.StatusOK {
				w.WriteHeader(context.StatusCode)
			}
			if context.RespBody != nil && len(context.RespBody) > 0 {
				w.Write(context.RespBody)
			}
		}

		var handleF func(pattern web.PatternType, handler web.HandlerType)
		switch m {
		case http.MethodGet:
			handleF = this.Mux.Get
		case http.MethodPost:
			handleF = this.Mux.Post
		case http.MethodHead:
			handleF = this.Mux.Head
		case http.MethodPut:
			handleF = this.Mux.Put
		case http.MethodDelete:
			handleF = this.Mux.Delete
		case http.MethodTrace:
			handleF = this.Mux.Trace
		case http.MethodOptions:
			handleF = this.Mux.Options
		case http.MethodConnect:
			handleF = this.Mux.Connect
		case http.MethodPatch:
			handleF = this.Mux.Patch
		}

		handleF(this.prefix+fixRouterPath(path), g)
	}
}

func (this *Router) NewRouterGroup() *Router {
	middlewares := make([]Middleware, len(this.Middlewares))
	copy(middlewares, this.Middlewares)

	return &Router{
		Mux:                 this.Mux,
		Middlewares:         middlewares,
		prefix:              this.prefix,
		isOptionsReqEnabled: this.isOptionsReqEnabled,
	}
}

func (this *Router) Run(port string) error {
	if len(port) == 0 {
		return errors.New("Port is not valid")
	}

	if port[:1] != ":" {
		port = ":" + port
	}

	return http.ListenAndServe(port, this.Mux)
}

func fixRouterPath(path string) string {
	if len(path) > 0 {
		if path == "/" {
			return path
		}

		if path[:1] != "/" {
			path = "/" + path
		}
		if path[len(path)-1:] == "/" {
			path = path[:len(path)-1]
		}
	}

	return path
}
