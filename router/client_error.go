package router

type ClientError map[string]interface{}

func NewClientError(message string, extraFields map[string]interface{}) ClientError {
	c := ClientError{}

	if extraFields != nil && len(extraFields) > 0 {
		for k, v := range extraFields {
			c[k] = v
		}
	}

	c["error"] = message

	return c
}

func (this ClientError) Error() string {
	error, _ := this["error"].(string)
	return error
}

func (this ClientError) Equals(err error) bool {
	return this.Error() == err.Error()
}

func (this ClientError) GetMap() map[string]interface{} {
	return map[string]interface{}(this)
}
