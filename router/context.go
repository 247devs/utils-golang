package router

import (
	"io/ioutil"
	"net/http"
	"net/url"

	"time"

	"errors"

	"github.com/satori/go.uuid"
	"golang.org/x/text/language"
	"gopkg.in/mgo.v2/bson"
)

var (
	default_lang = language.English
)

type UrlParams map[string]string

func (this UrlParams) GetObjectId(key string) (objectId bson.ObjectId) {
	if objectIdString := this[key]; bson.IsObjectIdHex(objectIdString) {
		return bson.ObjectIdHex(objectIdString)
	}

	return
}

type Context struct {
	Id          string
	middlewares []Middleware
	cFunc       ControllerFunc
	index       int

	Req        *http.Request
	RespWriter http.ResponseWriter

	UrlQuery  url.Values
	UrlParams UrlParams

	ReqBody    []byte
	RespBody   []byte
	StatusCode int

	Response interface{}
	Error    error

	Values map[string]interface{}
}

func NewContext(
	r *http.Request,
	w http.ResponseWriter,
	urlParams map[string]string,
	middlewares []Middleware,
	cFunc ControllerFunc,
) *Context {

	reqBody, _ := ioutil.ReadAll(r.Body)

	return &Context{
		Id:          uuid.Must(uuid.NewV4()).String(),
		middlewares: middlewares,
		cFunc:       cFunc,

		StatusCode: http.StatusOK,

		Req:        r,
		RespWriter: w,

		UrlQuery:  r.URL.Query(),
		UrlParams: urlParams,

		ReqBody: reqBody,
		Values:  map[string]interface{}{},
	}

}

func (this *Context) execControllerFunc() {
	this.index++
	this.Response, this.Error = this.cFunc(this)
}

func (this *Context) Do() {
	if this.index == len(this.middlewares) {
		this.execControllerFunc()
	} else if this.index < len(this.middlewares) {
		this.middlewares[this.index](this)
		this.Next()
	}
}

func (this *Context) Next() {
	this.index++
	this.Do()
}

func (this *Context) Stop() {
	this.index = len(this.middlewares) + 1
}

func GetTimeZoneLocationFromContext(c *Context) (loc *time.Location, err error) {
	tz := c.Req.Header.Get("Time-Zone")
	if loc, err = time.LoadLocation(tz); err != nil {
		err = errors.New("invalid_time_zone_header")
	}

	return
}

func GetFirstLangTagInAcceptLang(acceptLanguage string) language.Tag {
	tag, _, _ := language.ParseAcceptLanguage(acceptLanguage)
	if len(tag) == 0 {
		return default_lang
	} else {
		return tag[0]
	}
}

func GetFirstLangBaseInAcceptLang(acceptLanguage string) language.Base {
	tag := GetFirstLangTagInAcceptLang(acceptLanguage)

	base, _ := tag.Base()
	return base
}

func GetFirstLangTagInContext(c *Context) (tag language.Tag) {
	return GetFirstLangTagInAcceptLang(c.Req.Header.Get("Accept-Language"))
}

func GetFirstLangBaseInContext(c *Context) (base language.Base) {
	return GetFirstLangBaseInAcceptLang(c.Req.Header.Get("Accept-Language"))
}
