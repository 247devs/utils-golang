package middleware

import (
	"net/http"
	"runtime/debug"

	"bitbucket.org/247devs/utils-golang/router"
	"github.com/sirupsen/logrus"
)

func RecoverPanicWithLogrus() func(c *router.Context) {
	return RecoverPanic(func(stack string, recover interface{}) {
		logrus.WithFields(logrus.Fields{
			"stack":   string(debug.Stack()),
			"recover": recover,
		}).Panic("Panic recovered")
	})
}

func RecoverPanic(logFunction func(stack string, recover interface{})) func(c *router.Context) {
	return func(c *router.Context) {
		defer func() {
			if r := recover(); r != nil {
				if logFunction != nil {
					logFunction(string(debug.Stack()), r)
				}

				c.StatusCode = http.StatusInternalServerError
			}
		}()

		c.Next()
	}
}
