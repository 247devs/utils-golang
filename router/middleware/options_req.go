package middleware

import (
	"net/http"
	"strings"

	"bitbucket.org/247devs/utils-golang/router"
)

func OptionsReq(allowOrigin string, allowHeaders ...string) router.Middleware {
	return func(c *router.Context) {
		if len(allowOrigin) > 0 {
			c.RespWriter.Header().Set("Access-Control-Allow-Origin", allowOrigin)
		}

		c.RespWriter.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE, PATCH")

		if len(allowHeaders) > 0 {
			c.RespWriter.Header().Set("Access-Control-Allow-Headers", strings.Join(allowHeaders, ","))
		}

		if c.Req.Method == http.MethodOptions {
			c.Stop()
		}
	}
}
