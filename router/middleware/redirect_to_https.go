package middleware

import (
	"net/http"
	"strings"

	"bitbucket.org/247devs/utils-golang/router"
)

func RedirectToHttps(c *router.Context) {
	if proto := c.Req.Proto; len(proto) < 5 || strings.ToLower(proto[:5]) != "https" {
		http.Redirect(c.RespWriter, c.Req, "https://"+c.Req.Host+c.Req.URL.Path, http.StatusPermanentRedirect)

		c.Stop()
	}
}
