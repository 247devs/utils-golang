package middleware

import (
	"errors"
	"time"

	"bitbucket.org/247devs/utils-golang/router"
	"bitbucket.org/247devs/utils-golang/jwt"
	"bitbucket.org/247devs/utils-golang/unix_time"
)

type JtiInvalidator interface {
	InvalidateJti() error
}

func JWTAutorenew(secret string, encryptKey string, autorefreshBeforeExp time.Duration) router.Middleware {
	return func(c *router.Context) {
		jwtClaims, ok := c.Values["jwtClaims"].(jwt.JwtClaimsInterface)
		if !ok {
			unauthorizedResponse(c, errors.New("jwtClaims in context values isn't JwtClaimsInterface"))
			return
		}

		if unix_time.Now() > jwtClaims.GetExpiresAt().Add(-autorefreshBeforeExp) {
			duration := time.Duration(jwtClaims.GetExpiresAt()-jwtClaims.GetNotBefore()) * time.Millisecond
			newJwtHeader, err := jwt.NewJWTStringHeader(secret, encryptKey, jwtClaims.GetAudience(), jwtClaims.GetSubject(), duration, jwtClaims)
			if err != nil {
				c.Error = errors.New("jwt autorenew failed " + err.Error())
				c.Stop()
				return
			}

			if jtiInvalidator, ok := jwtClaims.(JtiInvalidator); ok {
				if err = jtiInvalidator.InvalidateJti(); err != nil {
					c.Error = errors.New("jti invalidator failed " + err.Error())
					c.Stop()
					return
				}
			}

			c.RespWriter.Header().Set("Access-Control-Expose-Headers", "Authorization")
			c.RespWriter.Header().Set("Authorization", newJwtHeader)
		}
	}
}
