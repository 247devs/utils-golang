package middleware

import (
	"strings"

	"bitbucket.org/247devs/utils-golang/router"
	"bitbucket.org/247devs/utils-golang/strings_utils"
)

func FixProxyRequest(c *router.Context) {
	if proto := c.Req.Header.Get("X-Forwarded-Proto"); len(proto) > 0 {
		c.Req.Proto = proto
	}

	if ips := c.Req.Header.Get("X-Forwarded-For"); len(ips) > 0 {
		c.Req.RemoteAddr = strings_utils.Trim(strings.Split(ips, ",")[0])
	}
}
