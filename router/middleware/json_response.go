package middleware

import (
	"encoding/json"

	"bitbucket.org/247devs/utils-golang/router"
)

func JsonResponse(c *router.Context) {
	c.Next()

	if c.Response != nil && c.Error == nil {
		if body, err := json.Marshal(c.Response); err != nil {
			c.Response = nil
			c.Error = err
		} else {
			c.RespBody = body
			c.RespWriter.Header().Set("Content-Type", "application/json; charset=utf-8")
		}
	}

	return
}
