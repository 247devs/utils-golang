package middleware

import (
	"net/http"

	"bitbucket.org/247devs/utils-golang/router"
)

func Error(c *router.Context) {
	c.Next()

	if c.Error != nil {
		c.Response = nil
		c.RespBody = nil
		if c.StatusCode == http.StatusOK {
			c.StatusCode = http.StatusInternalServerError
		}
	}
}

func ClientError(c *router.Context) {
	c.Next()

	if c.Error != nil {
		if clientError, ok := c.Error.(router.ClientError); ok {
			c.Response = clientError
			c.Error = nil
			c.StatusCode = http.StatusBadRequest
		}
	}
}
