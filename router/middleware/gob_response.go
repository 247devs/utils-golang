package middleware

import (
	"bytes"
	"encoding/gob"

	"bitbucket.org/247devs/utils-golang/router"
)

func GobResponse(c *router.Context) {
	c.Next()

	if c.Response != nil {
		buff := new(bytes.Buffer)
		if err := gob.NewEncoder(buff).Encode(c.Response); err != nil {
			c.Response = nil
			c.Error = err
		} else {
			c.RespBody = buff.Bytes()
			c.RespWriter.Header().Set("Content-Type", "application/x-gob")
		}
	}

	return
}
