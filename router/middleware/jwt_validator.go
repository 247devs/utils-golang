package middleware

import (
	"net/http"
	"reflect"

	"bitbucket.org/247devs/utils-golang/router"
	"bitbucket.org/247devs/utils-golang/jwt"
)

func unauthorizedResponse(c *router.Context, err error) {
	c.StatusCode = http.StatusUnauthorized
	if err != nil {
		c.Error = err
	}
	c.Stop()
}

type JtiValidator interface {
	CheckJtiIsValid() bool
}

func JWTValidator(secret string, decryptKey string, jwtType jwt.JwtClaimsInterface) router.Middleware {
	return func(c *router.Context) {
		var jwtClaims jwt.JwtClaimsInterface
		if jC, ok := c.Values["jwtClaims"]; ok {
			jwtClaims, _ = jC.(jwt.JwtClaimsInterface)
		}

		if jwtClaims == nil {
			if jwtString := c.Req.Header.Get("Authorization"); len(jwtString) > 0 {
				val := reflect.ValueOf(jwtType)
				if val.Kind() == reflect.Ptr {
					val = reflect.Indirect(val)
				}

				jwtClaims = reflect.New(val.Type()).Interface().(jwt.JwtClaimsInterface)
				err := jwt.GetJwtClaims(jwtString, secret, decryptKey, jwtClaims)
				if err != nil {
					unauthorizedResponse(c, err)
					return
				}
			} else {
				unauthorizedResponse(c, nil)
				return
			}
		}

		if err := jwtClaims.Valid(); err != nil {
			unauthorizedResponse(c, err)
			return
		}

		if jtiValidator, ok := jwtClaims.(JtiValidator); ok {
			if !jtiValidator.CheckJtiIsValid() {
				unauthorizedResponse(c, nil)
				return
			}
		}

		c.Values["jwtClaims"] = jwtClaims
	}
}
