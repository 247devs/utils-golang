package models

import "errors"

type ENV string

func (this ENV) IsValid() error {
	if this == Development || this == Staging || this == Production {
		return nil
	} else {
		return errors.New("env is not valid")
	}
}

func (this ENV) IsProduction() bool {
	return this == Production
}

func (this ENV) IsDevelopment() bool {
	return this == Development
}

func (this ENV) String() string {
	return string(this)
}

const (
	Development ENV = "development"
	Staging ENV = "staging"
	Production ENV = "production"
)