package models

import (
	"bitbucket.org/247devs/utils-golang/strings_utils"
	"github.com/pkg/errors"
)

var ErrInvalidName = errors.New("invalid_name")

type Name string

func (this Name) Format() Name {
	return Name(strings_utils.CleanName(this.String()))
}

func (this Name) Valid() error {
	if len(strings_utils.Trim(this.String())) != len(this.Format()) {
		return ErrInvalidName
	}

	return nil
}

func (this Name) String() string {
	return string(this)
}
