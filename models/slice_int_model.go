package models

import "sort"

type SliceInt []int

func (this SliceInt) Contains(num int) (result bool) {
	for _, n := range this {
		if n == num {
			return true
		}
	}

	return false
}

func (this SliceInt) Remove(num int) (newSlice SliceInt) {
	newSlice = make(SliceInt, 0, len(this))

	for _, n := range this {
		if n != num {
			newSlice = append(newSlice, n)
		}
	}

	return
}

func (this SliceInt) Sort(ascending bool) {
	var f func(i, j int) bool
	if ascending {
		f = func(i, j int) bool { return this[i] < this[j] }
	} else {
		f = func(i, j int) bool { return this[i] > this[j] }
	}

	sort.Slice(this, f)
}

func (this SliceInt) GetUniquesInt() (newSlice SliceInt) {
	switch len(this) {
	case 0:
		return
	case 1:
		return SliceInt{this[0]}
	case 2:
		if this[0] != this[1] {
			return SliceInt{this[0], this[1]}
		} else {
			return SliceInt{this[0]}
		}
	}

	newSlice = make(SliceInt, 0, len(this))

	mapSlice := make(map[int]bool, len(this))

	for _, s := range this {
		if _, ok := mapSlice[s]; !ok {
			mapSlice[s] = true
			newSlice = append(newSlice, s)
		}
	}

	return
}
