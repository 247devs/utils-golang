package models

import (
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

func ConfigureS3ObjectKey(S3Client *s3.S3, S3Bucket string) {
	s3Client = S3Client
	s3Bucket = S3Bucket
}

var s3Client *s3.S3
var s3Bucket string

type S3ObjectKey string

func (this S3ObjectKey) String() string {
	return string(this)
}

func (this S3ObjectKey) FileName() string {
	split := strings.Split(this.String(), "/")
	return split[len(split)-1]
}

func (this S3ObjectKey) GetUrl() string {
	return "https://" + s3Bucket + ".s3.amazonaws.com/" + this.String()
}

func (this S3ObjectKey) GetPresignUrl(expire time.Duration) (string, error) {
	req, _ := s3Client.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(s3Bucket),
		Key:    aws.String(this.String()),
	})
	return req.Presign(expire)
}
