package models

import (
	"bitbucket.org/247devs/utils-golang/unix_time"
	"gopkg.in/mgo.v2/bson"
	"bitbucket.org/247devs/utils-golang/errors_custom"
)

type DatesRange []unix_time.UnixTime

func NewDatesRange(startRange unix_time.UnixTime, endRange unix_time.UnixTime) (datesRange DatesRange, err error) {
	datesRange = DatesRange{startRange, endRange}
	if !datesRange.IsValid() {
		err = errors_custom.ErrInvalidParameters
	}

	return
}

func (this DatesRange) IsValid() bool {
	if len(this) != 2 {
		return false
	}

	if this[0].IsEmpty() || this[1].IsEmpty(){
		return false
	}

	if this[0] < 0 || this[1] < 0 {
		return false
	}

	if this[0] >= this[1] {
		return false
	}

	return true
}

func (this DatesRange) ToBsonMForFind(fieldStartRange string, fieldEndRange string) (bsonM bson.M) {
	if this.IsValid() {
		internalFind := bson.M{
			"$gte" : this[0],
			"$lte" : this[1],
		}

		bsonM = bson.M{"$or" : []bson.M{
			{
				fieldStartRange : internalFind,
			},
			{
				fieldEndRange : internalFind,
			},
			{
				fieldStartRange : bson.M{"$lt" : this[0]},
				fieldEndRange : bson.M{"$gt" : this[1]},
			},
		}}
	}

	return
}

func (this DatesRange) Extend(datesRange DatesRange) (newDatesRange DatesRange, err error) {
	if !this.IsValid() || !datesRange.IsValid() {
		err = errors_custom.ErrInvalidParameters
		return
	}

	newDatesRange = this
	if datesRange[0] < newDatesRange[0] {
		newDatesRange[0] = datesRange[0]
	}
	if datesRange[1] > newDatesRange[1] {
		newDatesRange[1] = datesRange[1]
	}

	if !newDatesRange.IsValid() {
		err = errors_custom.ErrInvalidParameters
	}

	return
}

func (this DatesRange) CommonRange(datesRange DatesRange) (newDatesRange DatesRange, err error) {
	if !this.IsValid() || !datesRange.IsValid() {
		err = errors_custom.ErrInvalidParameters
		return
	}

	newDatesRange = append(DatesRange{}, this...)
	if datesRange[0] > newDatesRange[0] {
		newDatesRange[0] = datesRange[0]
	}
	if datesRange[1] < newDatesRange[1] {
		newDatesRange[1] = datesRange[1]
	}

	if !newDatesRange.IsValid() {
		err = errors_custom.ErrInvalidParameters
	}

	return
}