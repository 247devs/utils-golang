package models

import "errors"

var ErrInvalidGender = errors.New("invalid_gender")

type Gender string

const (
	Undefined Gender = "UNDEFINED"
	Male      Gender = "MALE"
	Female    Gender = "FEMALE"
)

func (this Gender) IsValid() bool {
	return this.Valid() == nil
}

func (this Gender) Valid() error {
	switch this {
	case Undefined, Male, Female:
		return nil
	default:
		return ErrInvalidGender
	}
}

func (this Gender) String() string {
	return string(this)
}
