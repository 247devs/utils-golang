package models

import (
	"errors"

	"github.com/dannyvankooten/vat"
)

var ErrInvalidVatFormat = errors.New("invalid_vat_format")

type VatNumber string

func (this VatNumber) Valid(checkIsFiscalCode bool) (err error) {
	if valid, _ := vat.ValidateNumberFormat(string(this)); !valid {
		if checkIsFiscalCode {
			if err = FiscalCode(this).Valid(); err != nil {
				return ErrInvalidVatFormat
			}
		} else {
			return ErrInvalidVatFormat
		}
	}

	return
}

func (this VatNumber) String() string {
	return string(this)
}
