package models

import (
	"errors"
	"regexp"
)

var regexFiscalCode = regexp.MustCompile(`(?i)^(?:(?:[B-DF-HJ-NP-TV-Z]|[AEIOU])[AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[1256LMRS][\dLMNP-V])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$`)

var ErrInvalidFiscalCode = errors.New("invalid_fiscal_code")

type FiscalCode string

func (this FiscalCode) Valid() (err error) {
	if !regexFiscalCode.MatchString(string(this)) {
		return ErrInvalidFiscalCode
	}

	return
}

func (this FiscalCode) String() string {
	return string(this)
}
