package models

type ClientOS string

func (this ClientOS) IsValid() bool {
	return this == ANDROID || this == IOS
}

const(
	ANDROID ClientOS = "android"
	IOS ClientOS = "ios"
)
