package models

import (
	"encoding/json"
	"errors"
	"net"

	"strings"

	"github.com/asaskevich/govalidator"
)

var ErrInvalidEmail = errors.New("invalid_email")

var CleanEmailOnJsonUnmarshal = true

type Email string

func (this Email) Valid(lookupMX bool) (err error) {
	if !this.ValidWithRegex() {
		return ErrInvalidEmail
	}

	if lookupMX && !this.ValidWithLookupMX() {
		return ErrInvalidEmail
	}

	return
}

func (this *Email) UnmarshalJSON(b []byte) (err error) {
	emailStr := ""
	if err = json.Unmarshal(b, &emailStr); err != nil {
		return
	}

	*this = Email(emailStr)
	if CleanEmailOnJsonUnmarshal {
		*this = this.Clean()
	}

	return
}

func (this Email) ValidWithRegex() (isValid bool) {
	return govalidator.IsEmail(this.String())
}

func (this Email) ValidWithLookupMX() (isValid bool) {
	_, host := this.Split()
	_, err := net.LookupMX(host)
	return err == nil
}

func (this Email) Split() (account, host string) {
	emailStr := this.String()
	i := strings.LastIndexByte(emailStr, '@')
	account = emailStr[:i]
	host = emailStr[i+1:]
	return
}

func (this Email) Clean() Email {
	return Email(strings.Replace(strings.ToLower(this.String()), " ", "", -1))
}

func (this Email) String() string {
	return string(this)
}
