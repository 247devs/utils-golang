package models

import (
	"strconv"
	"strings"
)

type Price float64

func (this Price) String() string {
	return strings.Replace(strconv.FormatFloat(this.Round().ToFloat64(), 'f', 2, 64), ".", ",", 1)
}

func (this Price) Round() Price {
	return Price(int(this * 100)) / 100
}

func (this Price) ToFloat64() float64 {
	return float64(this)
}