package models

var ADDRESS_FIELD = struct {
	PLACE_ID      string
	DESCRIPTION   string
	STREET        string
	STREET_NUMBER string
	CITY          string
	COUNTRY       string
	COUNTRY_CODE  string
	REGION        string
	POSTAL_CODE   string
	COORDINATES   string
}{
	PLACE_ID:      "placeId",
	DESCRIPTION:   "description",
	STREET:        "street",
	STREET_NUMBER: "streetNumber",
	CITY:          "city",
	COUNTRY:       "country",
	COUNTRY_CODE:  "countryCode",
	REGION:        "region",
	POSTAL_CODE:   "postalCode",
	COORDINATES:   "coordinates",
}

type Address struct {
	PlaceId      string      `json:"placeId,omitempty" bson:"placeId,omitempty"`
	Description  string      `json:"description,omitempty" bson:"description,omitempty"`
	Street       string      `json:"street,omitempty" bson:"street,omitempty"`
	StreetNumber string      `json:"streetNumber,omitempty" bson:"streetNumber,omitempty"`
	City         string      `json:"city,omitempty" bson:"city,omitempty"`
	Country      string      `json:"country,omitempty" bson:"country,omitempty"`
	CountryCode  string      `json:"countryCode,omitempty" bson:"countryCode,omitempty"`
	Province     string      `json:"province,omitempty" bson:"province,omitempty"`
	Region       string      `json:"region,omitempty" bson:"region,omitempty"`
	PostalCode   string      `json:"postalCode,omitempty" bson:"postalCode,omitempty"`
	Coordinates  Coordinates `json:"coordinates,omitempty" bson:"coordinates,omitempty"`
}
