package models

import (
	"errors"

	"strings"

	"github.com/asaskevich/govalidator"
)

var ErrInvalidPhoneNumber = errors.New("invalid_phone_number")

type PhoneNumber string

func (this PhoneNumber) Valid() error {
	if len(this) < 8 {
		return ErrInvalidPhoneNumber
	}

	if this[:1] != "+" {
		return ErrInvalidPhoneNumber
	}

	if !govalidator.IsNumeric(this.String()[1:]) {
		return ErrInvalidPhoneNumber
	}

	return nil
}

func (this PhoneNumber) Clean() PhoneNumber {
	s := strings.Replace(this.String(), " ", "", -1)

	if strings.HasPrefix(s, "00") {
		s = "+" + s[2:]
	}

	return PhoneNumber(s)
}

func (this PhoneNumber) HasInternationalPrefix() bool {
	return strings.HasPrefix(this.String(), "+")
}

func (this *PhoneNumber) AddInternationalPrefix(prefix string) {
	if len(*this) < 2 {
		return
	}

	if !this.HasInternationalPrefix() {
		*this = PhoneNumber(prefix + this.String())
	}
}

func (this PhoneNumber) String() string {
	return string(this)
}
