package models

import (
	"bitbucket.org/247devs/utils-golang/unix_time"
	"gopkg.in/mgo.v2/bson"
)

var BASE_FIELD = struct {
	ID         string
	CREATED_AT string
	UPDATED_AT string
	DELETED    string
	DELETED_AT string
}{
	ID:         "_id",
	CREATED_AT: "createdAt",
	UPDATED_AT: "updatedAt",
	DELETED:    "deleted",
	DELETED_AT: "deletedAt",
}

type BaseModel struct {
	Id        bson.ObjectId      `bson:"_id,omitempty" json:"id,omitempty"`
	CreatedAt unix_time.UnixTime `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
	UpdatedAt unix_time.UnixTime `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"`
	Deleted   bool               `bson:"deleted" json:"deleted,omitempty"`
	DeletedAt unix_time.UnixTime `bson:"deletedAt,omitempty" json:"deletedAt,omitempty"`
}

func (this *BaseModel) ToZero() {
	*this = BaseModel{}
}

func (this *BaseModel) InitBaseModel() {
	this.Id = bson.NewObjectId()
	now := unix_time.Now()
	this.CreatedAt = now
	this.UpdatedAt = now
}
