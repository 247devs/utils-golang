package models

import (
	"math"
	"strconv"
)

type Coordinates []float64 //[longitude, latitude]

func NewCoordinates(lng float64, lat float64) Coordinates {
	return Coordinates{lng, lat}
}

func (this Coordinates) GetLng() float64 {
	return this[0]
}

func (this Coordinates) GetLat() float64 {
	return this[1]
}

func (this Coordinates) IsValid() bool {
	if len(this) != 2 {
		return false
	}

	if this[0] < -180 || this[0] > 180 || this[1] < -90 || this[1] > 90 {
		return false
	}

	return true
}

func (this Coordinates) Distance(coords Coordinates) float64 {
	var la1, lo1, la2, lo2, r float64
	la1 = float64(this[1]) * math.Pi / 180
	lo1 = float64(this[0]) * math.Pi / 180
	la2 = float64(coords[1]) * math.Pi / 180
	lo2 = float64(coords[0]) * math.Pi / 180

	r = 6378100 // Earth radius in METERS

	hsin := func(theta float64) float64 {
		return math.Pow(math.Sin(theta/2), 2)
	}

	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)

	return 2 * r * math.Asin(math.Sqrt(h))
}

func (this Coordinates) Hash() (hash string) {
	lng := strconv.FormatFloat(float64(this[0]), 'f', -1, 32)
	lat := strconv.FormatFloat(float64(this[1]), 'f', -1, 32)

	return lng + "|" + lat
}

func (this Coordinates) RoundForArea() Coordinates {
	round := func(num float64) float64 {
		num = float64(int(num*100) * 10)
		if num < 0 {
			num -= 5
		} else {
			num += 5
		}

		return num / 1000
	}

	location := make(Coordinates, 2)
	location[0] = round(this[0])
	location[1] = round(this[1])

	return location
}
