package models

import "gopkg.in/mgo.v2/bson"

type SliceString []string
type SliceObjectId []bson.ObjectId

func (this SliceString) Contains(str string) (result bool) {
	if len(this) > 0 {
		for _, s := range this {
			if str == s {
				return true
			}
		}
	}

	return false
}

func (this SliceString) Remove(str ...string) (newSlice SliceString) {
	newSlice = SliceString{}

	if len(this) > 0 {
		if len(str) > 0 {
			mapStrToRemove := make(map[string]bool, len(str))
			for _, s := range str {
				mapStrToRemove[s] = true
			}

			newSlice = make(SliceString, 0, len(this))
			for _, s := range this {
				if _, ok := mapStrToRemove[s]; !ok {
					newSlice = append(newSlice, s)
				}
			}
		} else {
			newSlice = this
		}
	}

	return
}

func (this SliceString) GetUniquesStr() (newSlice SliceString) {
	newSlice = make(SliceString, 0, len(this))
	switch len(this) {
	case 0:
		return
	case 1:
		return append(newSlice, this[0])
	case 2:
		if this[0] != this[1] {
			return append(newSlice, this[0], this[1])
		} else {
			return append(newSlice, this[0])
		}
	}

	mapSlice := make(map[string]bool, len(this))

	for _, s := range this {
		if _, ok := mapSlice[s]; !ok {
			mapSlice[s] = true
			newSlice = append(newSlice, s)
		}
	}

	return
}

func (this SliceString) Intersect(str ...string) (intersect SliceString) {
	var minSize int
	if len(this) < len(str) {
		minSize = len(this)
	} else {
		minSize = len(str)
	}

	if minSize == 0 {
		return []string{}
	}

	intersect = make([]string, 0, minSize)
	mapS1 := make(map[string]bool, len(this))

	for _, s := range this {
		mapS1[s] = true
	}

	for _, s := range str {
		if _, ok := mapS1[s]; ok {
			intersect = append(intersect, s)
		}
	}

	return
}

func (this SliceString) ToSliceObjectId() (sliceObjId SliceObjectId) {
	sliceObjId = make(SliceObjectId, len(this))
	for i, s := range this {
		sliceObjId[i] = bson.ObjectIdHex(s)
	}

	return
}

func (this SliceObjectId) ToSliceString() (sliceStr SliceString) {
	sliceStr = make(SliceString, len(this))
	for i, id := range this {
		sliceStr[i] = id.Hex()
	}

	return
}

func (this SliceObjectId) Contains(id bson.ObjectId) (result bool) {
	return this.ToSliceString().Contains(id.Hex())
}

func (this SliceObjectId) Remove(ids ...bson.ObjectId) (newSlice SliceObjectId) {
	return this.ToSliceString().Remove(SliceObjectId(ids).ToSliceString()...).ToSliceObjectId()
}

func (this SliceObjectId) GetUniquesObjectId() (newSlice SliceObjectId) {
	return this.ToSliceString().GetUniquesStr().ToSliceObjectId()
}
