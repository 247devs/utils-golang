package google_maps

import (
	"context"

	"strings"

	"errors"

	"bitbucket.org/247devs/utils-golang/models"
	"bitbucket.org/247devs/utils-golang/errors_custom"
	"bitbucket.org/247devs/utils-golang/strings_utils"
	"googlemaps.github.io/maps"
)

type GoogleMapsClient interface {
	CompleteAddressDetails(address *models.Address) (err error)
	Prediction(input string, predictionType PredictionType, location *models.Coordinates, radius *uint) (addressPredictions []models.Address, err error)
}

type PredictionType string

const (
	ALL     PredictionType = "all"
	PLACE   PredictionType = "place"
	ADDRESS PredictionType = "address"
)

func (this PredictionType) IsValid() bool {
	switch this {
	case ALL, PLACE, ADDRESS:
		return true
	default:
		return false
	}
}

type googleMapsClient struct {
	client *maps.Client
}

func NewGoogleMapsClient(apiKey string) GoogleMapsClient {
	c, err := maps.NewClient(maps.WithAPIKey(apiKey))
	if err != nil {
		panic(err)
	}

	return &googleMapsClient{c}
}

var ErrWrongPostalCode = errors.New("wrong_postal_code")

func (this *googleMapsClient) Prediction(input string, predictionType PredictionType, location *models.Coordinates, radius *uint) (addresses []models.Address, err error) {
	if len(input) == 0 {
		return nil, errors.New("invalid input")
	}

	if !predictionType.IsValid() {
		return nil, errors.New("invalid prediction type")
	}

	r := maps.PlaceAutocompleteRequest{
		Input:        input,
		Language:     "it",
		SessionToken: maps.NewPlaceAutocompleteSessionToken(),
	}

	if predictionType == ADDRESS {
		r.Types = "address"
	}

	if location != nil {
		r.Location = &maps.LatLng{
			Lng: location.GetLng(),
			Lat: location.GetLat(),
		}

		if radius != nil {
			r.Radius = *radius
		}
	}

	resp, err := this.client.PlaceAutocomplete(context.Background(), &r)
	if err != nil {
		if strings.Contains(err.Error(), "ZERO_RESULTS") {
			err = nil
		}
		return
	}

	addresses = make([]models.Address, 0, len(resp.Predictions))
	for _, p := range resp.Predictions {
		for _, t := range p.Types {
			if predictionType != PLACE || (t == "neighborhood" || t == "locality" || t == "route" || t == "political") {
				addresses = append(addresses, models.Address{
					Description: p.Description,
					PlaceId:     p.PlaceID,
				})

				break
			}
		}
	}

	return
}

func (this *googleMapsClient) CompleteAddressDetails(address *models.Address) (err error) {
	if address == nil || len(address.PlaceId) == 0 {
		return errors_custom.ErrInvalidParameters
	}

	r := maps.PlaceDetailsRequest{
		PlaceID:      address.PlaceId,
		Language:     "it",
		SessionToken: maps.NewPlaceAutocompleteSessionToken(),
	}

	resp, err := this.client.PlaceDetails(context.Background(), &r)
	if err != nil {
		return
	}

	address.Coordinates = models.NewCoordinates(resp.Geometry.Location.Lng, resp.Geometry.Location.Lat)

	for _, c := range resp.AddressComponents {
		for _, t := range c.Types {
			if len(c.LongName) > 0 {
				switch t {
				case "route":
					address.Street = c.LongName
				case "street_number":
					address.StreetNumber = c.LongName
				case "administrative_area_level_3":
					address.City = c.LongName
				case "administrative_area_level_2":
					address.Province = c.LongName
				case "administrative_area_level_1":
					address.Region = c.LongName
				case "country":
					address.Country = c.LongName
					address.CountryCode = c.ShortName
				case "postal_code":
					if len(address.PostalCode) > 0 && strings_utils.StringsToLowerWithoutSpace(address.PostalCode) != strings_utils.StringsToLowerWithoutSpace(c.LongName) {
						return ErrWrongPostalCode
					}

					address.PostalCode = c.LongName
				}
			}
		}
	}

	if len(address.Description) == 0 {
		if models.SliceString(resp.Types).Contains("establishment") && len(resp.Name) > 0 {
			address.Description = resp.Name + ", "
		}

		address.Description += resp.FormattedAddress
	}

	return
}
