package google_maps

import (
	"bitbucket.org/247devs/utils-golang/models"
	"bitbucket.org/247devs/utils-golang/errors_custom"
)

type googleMapsClientMock struct{}

func NewGoogleMapsClientMock() GoogleMapsClient {
	return &googleMapsClientMock{}
}

func (this *googleMapsClientMock) Prediction(input string, predictionType PredictionType, location *models.Coordinates, radius *uint) (addresses []models.Address, err error) {
	return []models.Address{
		{
			PlaceId:     "EjBWaWEgQ29zdGFuemEgQmF1ZGFuYSBWYWNjb2xpbmksIFJvbWEsIFJNLCBJdGFsaWE",
			Description: "Via Costanza Baudana Vaccolini, Roma, RM, Italia",
		},
	}, nil
}

func (this *googleMapsClientMock) CompleteAddressDetails(address *models.Address) (err error) {
	if address != nil {
		return errors_custom.ErrInvalidParameters
	}

	address.Street = "Via Costanza Baudana Vaccolini"
	address.StreetNumber = "14"
	address.City = "Roma"
	address.Region = "Lazio"
	address.Country = "Italia"
	address.CountryCode = "IT"
	address.PostalCode = "00153"
	address.Coordinates = models.Coordinates{12.466454, 41.878051}

	return nil
}
