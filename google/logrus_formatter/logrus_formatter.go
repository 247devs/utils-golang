package logrus_formatter

import (
	"encoding/json"
	"fmt"
	"time"

	"strings"

	"github.com/sirupsen/logrus"
)

func NewGCloudFormatter(timestampFormat string) *gCloudFormatter {
	return &gCloudFormatter{
		TimestampFormat: timestampFormat,
	}
}

type gCloudFormatter struct {
	// TimestampFormat sets the format used for marshaling timestamps.
	TimestampFormat string
}

func (f *gCloudFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	data := make(logrus.Fields, len(entry.Data)+3)
	for k, v := range entry.Data {
		switch v := v.(type) {
		case error:
			// Otherwise errors are ignored by `encoding/json`
			// https://github.com/Sirupsen/logrus/issues/137
			data[k] = v.Error()
		default:
			data[k] = v
		}
	}
	prefixFieldClashes(data)

	timestampFormat := f.TimestampFormat
	if timestampFormat == "" {
		timestampFormat = time.RFC3339
	}

	data["time"] = entry.Time.Format(timestampFormat)
	data["message"] = entry.Message
	if entry.Level == logrus.PanicLevel || entry.Level == logrus.FatalLevel {
		data["message"] = strings.ToUpper(entry.Level.String()) + " - " + entry.Message
		data["severity"] = "CRITICAL"
	} else {
		data["severity"] = strings.ToUpper(entry.Level.String())
	}

	serialized, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("Logger failed to marshal fields to JSON, %v. Message: %v", err, entry.Message)
	}
	return append(serialized, '\n'), nil
}

func prefixFieldClashes(data logrus.Fields) {
	if t, ok := data["time"]; ok {
		data["fields.time"] = t
	}

	if l, ok := data["level"]; ok {
		data["fields.level"] = l
		delete(data, "level")
	}

	if m, ok := data["message"]; ok {
		data["fields.message"] = m
	}

	if l, ok := data["severity"]; ok {
		data["fields.severity"] = l
	}
}
