package errors_custom

var ErrNoToken = NewClientError("no_token", 401, false, nil)
var ErrInvalidToken = func(token string) *ClientError {
	return NewClientError("invalid_token " + token, 401, false, nil)
}