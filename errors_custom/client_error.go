package errors_custom

import "gopkg.in/mgo.v2/bson"

type ClientErrorInterface interface {
	error
	SetResponse(interface{})
	GetResponse() interface{}
	GetResponseCode() int
}

type ClientError struct {
	error
	ClientErrorInterface
	msg string
	responseCode int
	resp interface{}
}

func NewClientError(message string, responseCode int, returnMessage bool, info bson.M) *ClientError {
	if len(message) == 0 {
		message = "generic_error"
	}

	clientError := ClientError{
		msg: message,
		responseCode: responseCode,
	}

	if info == nil {
		info = bson.M{}
	}

	if returnMessage {
		info["error"] = message
	}

	if len(info) > 0 {
		clientError.resp = info
	}

	return &clientError
}

func (this *ClientError) Error() string {
	return this.msg
}

func (this *ClientError) SetResponse(resp interface{}) {
	this.resp = resp
}

func (this *ClientError) GetResponse() interface{} {
	return this.resp
}

func (this *ClientError) GetResponseCode() int {
	return this.responseCode
}


