package errors_custom

import "errors"

var ErrInvalidDBModel = errors.New("invalid_db_model")