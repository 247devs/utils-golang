package errors_custom

import "errors"

var ErrHandlerNotFound = NewClientError("handler_not_found", 404, true, nil)

var ErrGenericError = errors.New("generic_error")

var ErrInvalidParameters = errors.New("invalid_parameters")
var ErrInvalidUserAgent = errors.New("invalid_user_agent")
var ErrInvalidObjectId = errors.New("invalid_object_id")
var ErrSecurityIssue = errors.New("security_issue")
var ErrThirdPartyUnexpected = errors.New("third_party_unexpected")

var ErrUserNotAuthorizedSeeThisContent = errors.New("user_not_authorized_see_this_content")

var ErrAlreadyExists = NewClientError("already_exists", 400, true, nil)
var ErrObjectNotExists = NewClientError("object_not_exists", 410, false, nil)
var ErrClientShouldRemove = NewClientError("client_should_remove", 410, false, nil)

var ErrGoogleKeyDoesntFound = NewClientError("google_key_doesnt_found", 400, false, nil)

var ErrServerNameNotSupported = errors.New("server_name_not_supported")
var ErrInvalidRpcRequest = errors.New("invalid_rpc_request")