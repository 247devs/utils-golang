package redis

import (
	"encoding/json"
	"errors"
	"time"

	"github.com/garyburd/redigo/redis"
)

type RedisDB interface {
	Do(commandName string, args ...interface{}) (reply interface{}, err error)
	Exists(key string) (exists bool, err error)
	AddExpire(key string, expire int64) error
	Set(key string, obj interface{}) error
	SetEx(key string, expire int64, obj interface{}) error
	Get(key string, result interface{}) error
	Delete(keys ...string) error
	Ping() error

	Pool() *redis.Pool
}

type redisDB struct {
	pool   *redis.Pool
	prefix string
}

func NewRedisDB(prefix string) RedisDB {
	return NewRedisDBWithAddress(":6379", prefix)
}

func NewRedisDBWithAddress(address string, prefix string, option ...redis.DialOption) RedisDB {
	r := redisDB{}
	r.prefix = prefix
	r.pool = &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			var c redis.Conn
			var err error
			if address[:5] == "redis" {
				c, err = redis.DialURL(address)
			} else {
				c, err = redis.Dial("tcp", address, option...)
			}
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}

	return &r
}

var RedisNotFound = errors.New("redis_not_found")
var RedisKeyNotValid = errors.New("redis_key_not_valid")

func (this *redisDB) Pool() *redis.Pool {
	return this.pool
}

func (this *redisDB) Do(commandName string, args ...interface{}) (reply interface{}, err error) {
	conn := this.pool.Get()
	defer conn.Close()

	return conn.Do(commandName, args...)
}

func (this *redisDB) Exists(key string) (exists bool, err error) {
	reply, err := this.Do("EXISTS", this.prefix+key)
	if err != nil {
		return
	}

	return reply.(int64) == 1, nil
}

func (this *redisDB) Ping() (err error) {
	_, err = this.Do("PING")
	return
}

func (this *redisDB) AddExpire(key string, expire int64) error {
	if len(key) == 0 {
		return RedisKeyNotValid
	}

	_, err := this.Do("EXPIRE", this.prefix+key, expire)
	return err
}

func (this *redisDB) Set(key string, obj interface{}) error {
	if len(key) == 0 {
		return RedisKeyNotValid
	}

	objSerial, err := json.Marshal(obj)
	if err == nil {
		_, err = this.Do("SET", this.prefix+key, objSerial)
	}
	return err
}

func (this *redisDB) SetEx(key string, expire int64, obj interface{}) error {
	if len(key) == 0 {
		return RedisKeyNotValid
	}

	objSerial, err := json.Marshal(obj)
	if err == nil {
		_, err = this.Do("SETEX", this.prefix+key, expire, objSerial)
	}
	return err
}

func (this *redisDB) Get(key string, result interface{}) error {
	if len(key) == 0 {
		return RedisKeyNotValid
	}

	reply, err := this.Do("GET", this.prefix+key)
	if err != nil {
		return err
	}

	if reply == nil {
		return RedisNotFound
	}
	return json.Unmarshal(reply.([]byte), result)
}

func (this *redisDB) Delete(keys ...string) error {
	keysInterface := make([]interface{}, 0, len(keys))
	for _, k := range keys {
		if len(k) > 0 {
			keysInterface = append(keysInterface, this.prefix+k)
		}
	}

	if len(keysInterface) == 0 {
		return RedisKeyNotValid
	}

	_, err := this.Do("DEL", keysInterface...)
	return err
}
