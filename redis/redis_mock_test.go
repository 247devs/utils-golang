package redis

import (
	"testing"
	"github.com/stretchr/testify/suite"
	"time"
)

var (
	RedisMock RedisDB

	key = "key"
	value = "value"
	expire int64 = 3
)

type MainSuite struct {
	suite.Suite
}

func (suite *MainSuite) SetupTest() {
	RedisMock = NewRedisDBMock("")
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(MainSuite))
}

func (suite *MainSuite) Test() {
	err := RedisMock.Set(key, &value)
	suite.Assert().Nil(err, "should return no error")

	result := ""
	err = RedisMock.Get(key, &result)
	suite.Assert().Nil(err, "should return no error")
	suite.Assert().Equal(value, result, "should return exact value")

	exists, err := RedisMock.Exists(key)
	suite.Assert().Nil(err, "should return no error")
	suite.Assert().True(exists, "should return true")

	err = RedisMock.Delete(key)
	suite.Assert().Nil(err, "should return no error")

	result = ""
	err = RedisMock.Get(key, &result)
	suite.Assert().NotNil(err, "should return no error")
	suite.Assert().Equal(0, len(result), "should return empty value")

	exists, err = RedisMock.Exists(key)
	suite.Assert().NotNil(err, "should return no error")
	suite.Assert().False(exists, "should return true")

	err = RedisMock.SetEx(key, expire, &value)
	suite.Assert().Nil(err, "should return no error")

	exists, err = RedisMock.Exists(key)
	suite.Assert().True(exists, "should return true")

	time.Sleep(time.Duration(expire) * time.Second)
	exists, err = RedisMock.Exists(key)
	suite.Assert().False(exists, "should return true")
	suite.Assert().NotNil(err, "should return error")

	err = RedisMock.Set(key, &value)
	suite.Assert().Nil(err, "should return no error")
	exists, err = RedisMock.Exists(key)
	suite.Assert().True(exists, "should return true")
	RedisMock.AddExpire(key, expire)
	time.Sleep(time.Duration(expire) * time.Second)
	exists, err = RedisMock.Exists(key)
	suite.Assert().False(exists, "should return true")
	suite.Assert().NotNil(err, "should return error")
}