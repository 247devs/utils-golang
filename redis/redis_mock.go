package redis

import (
	"encoding/json"
	"time"

	"bitbucket.org/247devs/utils-golang/unix_time"
	"github.com/garyburd/redigo/redis"
	"github.com/pkg/errors"
)

type val struct {
	val []byte
	exp unix_time.UnixTime
}

type redisMock struct {
	values map[string]val
	prefix string
}

func NewRedisDBMock(prefix string) RedisDB {
	return &redisMock{
		values: map[string]val{},
		prefix: prefix,
	}
}

func (this redisMock) Pool() *redis.Pool {
	return &redis.Pool{}
}

func (this redisMock) Do(commandName string, args ...interface{}) (reply interface{}, err error) {
	return nil, errors.New("Do is not support for redis mock")
}

func (this redisMock) getVal(key string) (obj val, err error) {
	obj, exists := this.values[this.prefix+key]
	if !exists {
		return val{}, RedisNotFound
	}

	if !obj.exp.IsEmpty() && obj.exp < unix_time.Now() {
		this.Delete(key)
		return val{}, RedisNotFound
	}

	return
}

func (this redisMock) setVal(key string, obj val) {
	this.values[this.prefix+key] = obj
}

func (this redisMock) Exists(key string) (exists bool, err error) {
	_, err = this.getVal(key)
	if err == nil {
		exists = true
	}

	return
}

func (this redisMock) AddExpire(key string, expire int64) (err error) {
	obj, err := this.getVal(key)
	if err != nil {
		return
	}

	obj.exp = unix_time.Now().Add(time.Duration(expire) * time.Second)

	this.setVal(key, obj)

	return nil
}

func (this redisMock) Set(key string, obj interface{}) (err error) {
	value, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	o := val{
		val: value,
	}

	this.setVal(key, o)

	return
}

func (this redisMock) SetEx(key string, expire int64, obj interface{}) (err error) {
	value, err := json.Marshal(obj)
	if err != nil {
		return
	}

	o := val{
		val: value,
		exp: unix_time.Now().Add(time.Duration(expire) * time.Second),
	}

	this.setVal(key, o)

	return
}

func (this redisMock) Get(key string, result interface{}) (err error) {
	o, err := this.getVal(key)
	if err != nil {
		return
	}

	if result != nil {
		err = json.Unmarshal(o.val, result)
	}

	return
}

func (this redisMock) Delete(keys ...string) error {
	for _, k := range keys {
		delete(this.values, this.prefix+k)
	}

	return nil
}

func (this redisMock) Ping() error {
	return nil
}
