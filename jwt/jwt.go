package jwt

import (
	"bitbucket.org/247devs/utils-golang/unix_time"
	jwt_go "github.com/dgrijalva/jwt-go"
	"time"
	"fmt"
	"errors"
	"gopkg.in/mgo.v2/bson"
	"bitbucket.org/247devs/utils-golang/strings_utils"
)

func NewJWTClaims(audience, subject string, duration time.Duration, jwtClaims JwtClaimsInterface) (err error) {
	if jwtClaims == nil {
		return errors.New("jwtClaims is nil")
	}

	now := unix_time.Now()
	jwtClaims.SetSubject(subject)
	jwtClaims.SetAudience(audience)
	jwtClaims.SetNotBefore(now)
	jwtClaims.SetIssuedAt(now)
	jwtClaims.SetExpiresAt(now.Add(duration))
	if len(jwtClaims.GetJti()) == 0 {
		jwtClaims.SetJti(bson.NewObjectId().Hex())
	}

	return jwtClaims.Valid()
}

func NewJWTString(secret, encryptKey, audience, subject string, duration time.Duration, jwtClaims JwtClaimsInterface) (jwt string, err error) {
	if err = NewJWTClaims(audience, subject, duration, jwtClaims); err != nil {
		return
	}

	token := jwt_go.NewWithClaims(jwt_go.SigningMethodHS256, jwtClaims)

	if jwt, err = token.SignedString([]byte(secret)); err != nil {
		return
	}

	if len(encryptKey) > 0 {
		if jwt, err = strings_utils.EncryptString(jwt, encryptKey); err != nil {
			return
		}
	}

	return
}

func NewJWTStringHeader(secret, encryptKey, audience, subject string, duration time.Duration, jwtClaims JwtClaimsInterface) (jwtHeader string, err error) {
	jwt, err := NewJWTString(secret, encryptKey, audience, subject, duration, jwtClaims)
	if err != nil {
		return
	}

	return "Bearer " + jwt, nil
}

func GetJwtClaims(jwtString, secret, decryptKey string, jwtClaims JwtClaimsInterface) (err error) {
	if len(jwtString) < 7 {
		return errors.New("Invalid JWT string")
	}
	if jwtClaims == nil {
		return errors.New("jwtClaims is nil")
	}
	if jwtString[:7] == "Bearer " {
		jwtString = jwtString[7:]
	}

	if len(decryptKey) > 0 {
		if jwtString, err = strings_utils.DecryptString(jwtString, decryptKey); err != nil {
			return
		}
	}

	parser := new(jwt_go.Parser)
	parser.SkipClaimsValidation = true
	_, err = parser.ParseWithClaims(jwtString, jwtClaims, func(token *jwt_go.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt_go.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(secret), nil
	})

	return
}