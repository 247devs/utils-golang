package jwt

import (
	"bitbucket.org/247devs/utils-golang/unix_time"
	"errors"
)

type JwtClaimsInterface interface {
	Valid() error

	GetJti() string
	SetJti(string)

	GetAudience() string
	SetAudience(string)

	GetSubject() string
	SetSubject(string)

	GetNotBefore() unix_time.UnixTime
	SetNotBefore(unix_time.UnixTime)

	GetIssuedAt() unix_time.UnixTime
	SetIssuedAt(unix_time.UnixTime)

	GetExpiresAt() unix_time.UnixTime
	SetExpiresAt(unix_time.UnixTime)
}

func NewJWTClaimsEmpty() *JwtClaims {
	return &JwtClaims{}
}

var JWT_CLAIMS_FIELD = struct {
	JTI string
	ISSUER string
	AUDIENCE string
	SUBJECT string
	EXPIRES_AT string
	ISSUED_AT string
	NOT_BEFORE string
} {
	JTI: "jti",
	ISSUER: "iss",
	AUDIENCE: "aud",
	SUBJECT: "sub",
	EXPIRES_AT: "exp",
	ISSUED_AT: "iat",
	NOT_BEFORE: "nbf",
}

type JwtClaims struct {
	Jti        string `json:"jti,omitempty" bson:"jti,omitempty"`
	Issuer    string `json:"iss,omitempty" bson:"iss,omitempty"`
	Audience  string `json:"aud,omitempty" bson:"aud,omitempty"`
	Subject   string `json:"sub,omitempty" bson:"sub,omitempty"`
	ExpiresAt unix_time.UnixTime  `json:"exp,omitempty" bson:"exp,omitempty"`
	IssuedAt  unix_time.UnixTime  `json:"iat,omitempty" bson:"iat,omitempty"`
	NotBefore unix_time.UnixTime  `json:"nbf,omitempty" bson:"nbf,omitempty"`
}

func (this *JwtClaims) GetSubject() string {
	return this.Subject
}

func (this *JwtClaims) SetSubject(sub string) {
	this.Subject = sub
}

func (this *JwtClaims) GetAudience() string {
	return this.Audience
}

func (this *JwtClaims) SetAudience(aud string) {
	this.Audience = aud
}

func (this *JwtClaims) GetNotBefore() unix_time.UnixTime {
	return this.NotBefore
}

func (this *JwtClaims) SetNotBefore(nbf unix_time.UnixTime) {
	this.NotBefore = nbf
}

func (this *JwtClaims) GetIssuedAt() unix_time.UnixTime {
	return this.IssuedAt
}

func (this *JwtClaims) SetIssuedAt(iat unix_time.UnixTime) {
	this.IssuedAt = iat
	}

func (this *JwtClaims) GetExpiresAt() unix_time.UnixTime {
	return this.ExpiresAt
}

func (this *JwtClaims) SetExpiresAt(exp unix_time.UnixTime) {
	this.ExpiresAt = exp
}

func (this *JwtClaims) GetJti() string {
	return this.Jti
}

func (this *JwtClaims) SetJti(jti string) {
	this.Jti = jti
}

func (this *JwtClaims) Valid() error {
	return ValidJwt(this)
}

func ValidJwt(jwt JwtClaimsInterface) error {
	if len(jwt.GetJti()) == 0 {
		return errors.New("Id isn't valid")
	}
	if len(jwt.GetSubject()) == 0 {
		return errors.New("Subject isn't valid")
	}
	if len(jwt.GetAudience()) == 0 {
		return errors.New("Audience isn't valid")
	}

	now := unix_time.Now()
	if jwt.GetExpiresAt() < now {
		return errors.New("Token is expired")
	}
	if jwt.GetNotBefore() > now {
		return errors.New("Token is not valid yet")
	}
	if jwt.GetNotBefore() > now {
		return errors.New("Token used before issued")
	}

	return nil
}