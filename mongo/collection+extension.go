package mongo

import (
	"bitbucket.org/247devs/utils-golang/unix_time"
	"bitbucket.org/247devs/utils-golang/errors_custom"
	"bitbucket.org/247devs/utils-golang/models"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2"
	"reflect"
)

type Collection struct {
	*mgo.Collection
	Super *mgo.Collection
}

func NewCollection(c *mgo.Collection) *Collection {
	return &Collection{
		c,
		c,
	}
}

func (this *Collection) Copy() *Collection {
	return NewDatabase(this.Database).Copy().C(this.Name)
}

func (this *Collection) Clone() *Collection {
	return NewDatabase(this.Database).Clone().C(this.Name)
}

func (this *Collection) Close() {
	this.Database.Session.Close()
}

// Deprecated: Use Close()
func (this *Collection) CloseSession() {
	this.Close()
}

func (this *Collection) Clear() (err error) {
	_, err = this.RemoveAll(nil)

	return
}

func (this *Collection) EnsureIndex(index mgo.Index) {
	if len(index.Key) == 0 {
		panic("Error Ensure Index is empty")
	} else {
		index.Key = append([]string{models.BASE_FIELD.DELETED}, index.Key...)
		if err := this.Super.EnsureIndex(index); err != nil {
			panic("Error Ensure Index " + err.Error())
		}
	}
}

func (this *Collection) EnsureIndexes(addDeletedIndex bool, indexes ...mgo.Index) {
	if addDeletedIndex {
		this.EnsureIndex(mgo.Index{
			Key: []string{models.BASE_FIELD.ID},
			Background: false,
		})
	}

	for _, index := range indexes {
		this.EnsureIndex(index)
	}

	return
}

func (this *Collection) FindId(id bson.ObjectId) *Query {
	find := bson.M{
		models.BASE_FIELD.ID : id,
	}

	q := this.Super.Find(CheckFind(find))
	return NewQuery(q)
}

func (this *Collection) Find(find bson.M) *Query {
	q := this.Super.Find(CheckFind(find))
	return NewQuery(q)
}

func (this *Collection) UpdateAll(find bson.M, update bson.M) (info *mgo.ChangeInfo, err error) {
	return this.Super.UpdateAll(CheckFind(find), CheckSet(update, false))
}

func (this *Collection) DeleteAll(find bson.M) (info *mgo.ChangeInfo, err error) {
	update := bson.M{}
	AddDeleteSetToBsonM(update)
	return this.Super.UpdateAll(CheckFind(find), update)
}

func (this *Collection) RemoveAllDeleted() (info *mgo.ChangeInfo, err error) {
	return this.RemoveAll(bson.M{models.BASE_FIELD.DELETED : true})
}

func (this *Collection) Insert(docs ...interface{}) error {
	for _, doc := range docs {
		elem := reflect.ValueOf(doc).Elem()

		id := elem.FieldByName("Id")
		createdAt := elem.FieldByName("CreatedAt")
		updatedAt := elem.FieldByName("UpdatedAt")
		if id.IsValid() && id.CanSet() &&
			createdAt.IsValid() && createdAt.CanSet() &&
			updatedAt.IsValid() && updatedAt.CanSet() {
			oldId, _ := id.Interface().(bson.ObjectId)
			if !oldId.Valid() {
				id.Set(reflect.ValueOf(bson.NewObjectId()))
			}
			now := unix_time.Now()
			oldCreatedAt, _ := createdAt.Interface().(unix_time.UnixTime)
			if oldCreatedAt.IsEmpty() {
				createdAt.Set(reflect.ValueOf(now))
			}
			updatedAt.Set(reflect.ValueOf(now))
		} else {
			return errors_custom.ErrInvalidDBModel
		}
	}

	return this.Super.Insert(docs...)
}

func (this *Collection) Bulk(typeBulk BulkType, unordered bool, updates ...interface{}) (err error) {
	for len(updates) > 0 {
		var updatesThisBulk []interface{}
		if len(updates) > 2000 {
			updatesThisBulk = updates[:2000]
			updates = updates[2000:]
		} else {
			updatesThisBulk = updates
			updates = updates[len(updates):]
		}

		bulk := this.Super.Bulk()
		if unordered {
			bulk.Unordered()
		}

		switch typeBulk {
		case UPSERT:
			bulk.Upsert(updatesThisBulk...)
		case UPDATE:
			bulk.Update(updatesThisBulk...)
		}
		_, err = bulk.Run()
		if err != nil {
			return
		}
	}

	return
}