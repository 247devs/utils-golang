package mongo

import (
	"bitbucket.org/247devs/utils-golang/errors_custom"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Query struct {
	*mgo.Query
	Super *mgo.Query
}

func NewQuery(q *mgo.Query) *Query {
	return &Query{
		q,
		q,
	}
}

func (this *Query) Delete(result interface{}) (info *mgo.ChangeInfo, err error) {
	update := bson.M{}
	AddDeleteSetToBsonM(update)

	change := mgo.Change{
		Update: update,
		ReturnNew: true,
	}

	info, err = this.Super.Apply(change, result)

	return
}

func (this *Query) Skip(n int) *Query {
	return NewQuery(this.Super.Skip(n))
}

func (this *Query) Limit(n int) *Query {
	return NewQuery(this.Super.Limit(n))
}

func (this *Query) Sort(fields ...string) *Query {
	return NewQuery(this.Super.Sort(fields...))
}

func (this *Query) Select(selector interface{}) *Query {
	return NewQuery(this.Super.Select(selector))
}

func (this *Query) CheckExists() (exists bool, err error) {
	n, err := this.Count()
	return n > 0, err
}

func (this *Query) Apply(change mgo.Change, result interface{}) (info *mgo.ChangeInfo, err error) {
	if update, ok := change.Update.(bson.M); ok {
		update = CheckSet(update, change.Upsert)

		if err == nil {
			return this.Super.Apply(change, result)
		}
	}

	return nil, errors_custom.ErrInvalidDBUpdateObj
}
