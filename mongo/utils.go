package mongo

import (
	"gopkg.in/mgo.v2/bson"
	"bitbucket.org/247devs/utils-golang/models"
	"bitbucket.org/247devs/utils-golang/unix_time"
	"bitbucket.org/247devs/utils-golang/errors_custom"
)

type BulkType int
const (
	UPSERT BulkType = iota
	UPDATE
)

func CheckFind(find bson.M) bson.M {
	if find == nil {
		find = bson.M{}
	}

	find[models.BASE_FIELD.DELETED] = false

	return find
}

func CheckSet(update bson.M, upsert bool) bson.M {
	if upsert && update["$setOnInsert"] == nil {
		update["$setOnInsert"] = bson.M{}
	}

	if update["$set"] == nil {
		update["$set"] = bson.M{}
	}

	if setOnInsert, ok := update["$setOnInsert"].(bson.M); ok || !upsert {
		if set, ok := update["$set"].(bson.M); ok {
			now := unix_time.Now()
			set[models.BASE_FIELD.UPDATED_AT] = now
			update["$set"] = set

			if upsert {
				setOnInsert[models.BASE_FIELD.CREATED_AT] = now
				update["$setOnInsert"] = setOnInsert
			}

			return update
		}
	}

	panic(errors_custom.ErrInvalidDBUpdateObj)
}

func AddDeleteSetToBsonM(update bson.M) (deletedAt unix_time.UnixTime) {
	setUpdate := bson.M{}
	if set, ok := update["$set"]; ok {
		setUpdate = set.(bson.M)
	}

	setUpdate[models.BASE_FIELD.DELETED] = true
	setUpdate[models.BASE_FIELD.DELETED_AT] = unix_time.Now()
	update["$set"] = setUpdate

	return
}