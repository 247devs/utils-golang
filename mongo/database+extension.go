package mongo

import "gopkg.in/mgo.v2"

type Database struct {
	*mgo.Database
	Super *mgo.Database
}

func NewDatabase(db *mgo.Database) *Database {
	return &Database{
		db,
		db,
	}
}

func (this *Database) C(name string) *Collection {
	return NewCollection(this.Super.C(name))
}

func (this *Database) Copy() *Database {
	return NewSession(this.Session).Copy().DB(this.Name)
}

func (this *Database) Clone() *Database {
	return NewSession(this.Session).Clone().DB(this.Name)
}

func (this *Database) Close() {
	this.Session.Close()
}