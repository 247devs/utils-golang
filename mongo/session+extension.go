package mongo


import (
	"gopkg.in/mgo.v2"
	"net"
	"crypto/tls"
	"bitbucket.org/247devs/utils-golang/models"
)


var masterSession *Session

type Session struct {
	*mgo.Session
	Super *mgo.Session
}

func NewSession(s *mgo.Session) *Session {
	return &Session{
		s,
		s,
	}
}

// Deprecated: Use DialMongoDB
func NewDBSession(env models.ENV) *mgo.Session {
	return nil
}

func DialMongoDB(mode mgo.Mode, uri string) *Session {
	dialInfo, err := mgo.ParseURL(uri)
	if err != nil {
		panic(err)
	}
	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		return tls.Dial("tcp", addr.String(), &tls.Config{})
	}

	s, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		panic(err)
	}

	s.SetMode(mode, true)

	return NewSession(s)
}

func (this *Session) Copy() *Session {
	return NewSession(this.Super.Copy())
}

func (this *Session) Clone() *Session {
	return NewSession(this.Super.Clone())
}

func (this *Session) DB(name string) *Database {
	return NewDatabase(this.Super.DB(name))
}