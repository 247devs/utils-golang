package unix_time

import (
	"bytes"
	"gopkg.in/mgo.v2/bson"
)

type UnixTimeString UnixTime

func (this UnixTimeString) ToUnixTime() UnixTime {
	return UnixTime(this)
}

func (t UnixTimeString) MarshalJSON() ([]byte, error) {
	return []byte(t.ToUnixTime().String()), nil
}


func (t *UnixTimeString) UnmarshalJSON(b []byte) (err error) {
	buf := bytes.NewBuffer(b)
	unix, err := NewFromString(buf.String())
	if err != nil {
		return
	}

	*t = UnixTimeString(unix)
	return
}

func (t UnixTimeString) GetBSON() (interface{}, error) {
	return t.ToUnixTime().String(), nil
}

func (t *UnixTimeString) SetBSON(raw bson.Raw) error {
	var str string

	if err := raw.Unmarshal(&str); err != nil {
		return err
	}

	unix, err := NewFromString(str)
	if err != nil {
		return err
	}

	*t = UnixTimeString(unix)

	return nil
}
