package unix_time

import (
	"bytes"
	"strconv"
	"time"

	"gopkg.in/mgo.v2/bson"
)

type UnixTime int64

const EmptyUnixTime = UnixTime(0)

type Preposition int

const (
	EMPTY Preposition = iota
	DI
	PER
)

var romeLocation, _ = time.LoadLocation("Europe/Rome")

func Now() UnixTime {
	return TimeToUnix(time.Now())
}

func New(t int64) UnixTime {
	return UnixTime(t)
}

func NewFromString(s string) (t UnixTime, err error) {
	if len(s) > 0 {
		if s[:1] == "\"" {
			var tTime time.Time
			tTime, err = time.Parse(time.RFC3339, s[1:len(s)-1])
			if err != nil {
				return
			}
			t = TimeToUnix(tTime)
		} else {
			var tInt int64
			tInt, err = strconv.ParseInt(s, 10, 64)
			if err != nil {
				return
			}

			t = New(tInt)
		}
	}

	return
}

func TimeToUnix(t time.Time) UnixTime {
	return UnixTime(t.UnixNano() / 1000000)
}

func (t UnixTime) GetPointer() *UnixTime {
	return &t
}

func (t UnixTime) ToTime() time.Time {
	return t.ToTimeLocation(nil)
}

func (t UnixTime) ToTimeLocation(loc *time.Location) time.Time {
	if loc == nil {
		loc = time.UTC
	}

	return time.Unix(0, t.ToUnix()*int64(time.Millisecond)).In(loc)
}

func (t UnixTime) ToTimeRomeLocation() time.Time {
	return t.ToTimeLocation(romeLocation)
}

func (t UnixTime) ToUnix() int64 {
	return int64(t)
}

func (t UnixTime) ToTimestamp() int64 {
	return t.ToUnix() / 1000
}

func (t UnixTime) ToTimestampString() string {
	return strconv.FormatInt(int64(t.ToTimestamp()), 10)
}

func (t UnixTime) String() string {
	return strconv.FormatInt(int64(t), 10)
}

func (t UnixTime) ItalianDateString() string {
	if t.IsEmpty() {
		return ""
	}
	date := t.ToTime()
	return strconv.Itoa(date.Day()) + "/" + strconv.Itoa(int(date.Month())) + "/" + strconv.Itoa(int(date.Year()))
}

func (t UnixTime) ToTimeString() string {
	date := t.ToTime()
	hoursStr := strconv.Itoa(date.Hour())
	if len(hoursStr) == 1 {
		hoursStr = "0" + hoursStr
	}
	minutesStr := strconv.Itoa(date.Minute())
	if len(minutesStr) == 1 {
		minutesStr = "0" + minutesStr
	}

	return hoursStr + ":" + minutesStr
}

func (t UnixTime) IsEmpty() bool {
	return t == EmptyUnixTime
}

func (t UnixTime) MarshalJSON() ([]byte, error) {
	return []byte(t.String()), nil
}

func (t *UnixTime) UnmarshalJSON(b []byte) (err error) {
	buf := bytes.NewBuffer(b)
	*t, err = NewFromString(buf.String())
	return
}

func (t UnixTime) GetBSON() (interface{}, error) {
	return t.ToTime(), nil
}

func (t *UnixTime) SetBSON(raw bson.Raw) error {
	var tm time.Time

	if err := raw.Unmarshal(&tm); err != nil {
		return err
	}

	*t = TimeToUnix(tm)

	return nil
}

func (this UnixTime) ToMidnight() UnixTime {
	t := this.ToTime()
	t = time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC)
	return TimeToUnix(t)
}

func (this UnixTime) AddDate(years int, months int, days int) UnixTime {
	return TimeToUnix(this.ToTime().AddDate(years, months, days))
}

func (this UnixTime) Add(d time.Duration) UnixTime {
	return New(this.ToUnix() + d.Nanoseconds()/1000000)
}

func (this UnixTime) ToHuman(preposition Preposition) (humanStr string) {
	midnight := time.Now()
	midnight = time.Date(midnight.Year(), midnight.Month(), midnight.Day(), 0, 0, 0, 0, time.UTC)
	midnightDate := this.ToTime()
	midnightDate = time.Date(midnightDate.Year(), midnightDate.Month(), midnightDate.Day(), 0, 0, 0, 0, time.UTC)
	diffDays := (midnightDate.Unix() - midnight.Unix()) / (24 * 60 * 60)

	RomeLocation, _ := time.LoadLocation("Europe/Rome")
	localDate := this.ToTime().In(RomeLocation)

	formatTime := func(str string) string {
		if len(str) == 1 {
			return "0" + str
		}

		return str
	}

	switch preposition {
	case DI:
		humanStr = "di "
	case PER:
		humanStr = "per "
	}

	if diffDays == 0 {
		humanStr += "oggi"
	} else if diffDays == 1 {
		humanStr += "domani"
	} else if diffDays == 2 {
		humanStr += "dopodomani"
	} else {
		switch preposition {
		case EMPTY:
			humanStr = "il "
		case DI:
			humanStr = "del "
		case PER:
			humanStr = "per il "
		}
		humanStr += formatTime(strconv.Itoa(localDate.Day())) + "/" + formatTime(strconv.Itoa(int(localDate.Month())))
	}

	humanStr += " alle " + formatTime(strconv.Itoa(localDate.Hour())) + ":" + formatTime(strconv.Itoa(localDate.Minute()))
	return
}
